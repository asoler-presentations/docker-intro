## Die Docker-Plattform

![plattform](images/docker-plattform.png)


## Images

![image](images/image.jpg)

Notes: Ein Image ist eine Vorlage mit Anweisungen zum Erstellen eines Docker-Containers. Häufig basiert ein Image auf einem anderen Image, mit einigen zusätzlichen Anpassungen. Du kannst deine eigene Images erstellen oder nur die von anderen erstellten und in einer Registry veröffentlichten Images verwenden. Ein Image ist die Definition wie ein Container zu erstellen ist. Darin werden alle notwendige Informationen festgelegt, um den Container lauffähig zu bekommen.


### Beispiel
```
FROM alpine:latest
MAINTAINER alexandre@soler-sanandres.net
LABEL \
	Description="minidlna DLNA server docker image" \
	Version="latest"
ENV minidlna_CACHE_DIR=/root/.cache/minidlna \
    minidlna_VIDEOS_DIR=/videos \
    minidlna_MUSIC_DIR=/music \
    minidlna_PICTURES_DIR=/pictures
RUN apk --no-cache add bash curl minidlna tini
EXPOSE 8200/tcp 1900/udp
VOLUME ["${minidlna_CACHE_DIR}", "${minidlna_VIDEOS_DIR}", "${minidlna_MUSIC_DIR}", "${minidlna_PICTURES_DIR}"]
COPY container-start.sh /container-start.sh
RUN chmod 744 container-start.sh
CMD ["./container-start.sh" ]
```


### Mit Images arbeiten

- `docker build`
- `docker pull`


## Container

![container](images/container.jpg)

Notes: Ein Docker-Container ist die lauffähige Instanz eines bestimmten Images. Jeder gestartete Docker Container bekommt eine eindeutige IP-Adresse. Container besitzen einen höhen Grad an Isolation. Docker-Container sind von der außenwelt isoliert. Um sie von außen (Intranet / Internet) erreichbar zu machen nutzen wir Port-Weiterleitungen. Docker-Container sind voneinander isoliert. Um sie miteinander kommunizieren zu lassen, verwenden wir Docker-Netzwerke. Alle an Daten durchgeführten Operationen innerhalb des Containers gehen verloren, nachdem der Container gestoppt / gelöscht wird. Um Daten nicht zu verlieren verwenden wir Docker-Volumes


### Operationen mit Container

- `docker create`
- `docker run`
- `docker start`
- `docker stop`
- `docker rm`


## Netzwerke

![network](images/network.jpg)

Notes: Um Kommunikation zwischen Container zu ermöglichen, verwenden wir Docker-Netzwerke. Ein Docker-Netzwerk ist ein virtuelles Netzwerk. Alle Container innerhalb eines Docker-Netzwerk können miteinander kommunizieren, wie normaler Rechner in einem echten Netzwerk.


### Operationen mit Netzwerken

- `docker network create`
- `docker network rm`
- `docker network connect`
- parameter `--network` in `docker run`


## Volumes

![container](images/volumes.jpg)

Notes: Daten werden in Volumes gespeichert. Volumes befinden sich auf dem Host-System. Volumes können von docker automatisch verwaltet werden oder von uns manuell.


### Operationen mit Volumes

- `docker volume create`
- `docker volume rm`
- parameter `-v` in `docker run`
