## Compose
![conductor](images/conductor.jpg)
Notes: Compose ist ein Werkzeug zur Definition und Ausführung von Multi-Container-Docker-Anwendungen. Mit Compose verwenden Sie eine YAML-Datei zur Konfiguration der Dienste Ihrer Anwendung. Anschließend erstellen und starten Sie mit einem einzigen Befehl alle Dienste aus Ihrer Konfiguration heraus.


## Compose Datei
```
version: '3.7'
 
services:
  db:
    image: mysql:5.7
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
 
  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress

volumes:
  db_data:
```


### Compose Operationen
- `docker-compose up`
- `docker-compose down`
