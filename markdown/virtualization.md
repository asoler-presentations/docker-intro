# Was ist

![docker](images/docker.png)

Notes: Docker ist eine Software zu Erstellung, Verwaltung und Ausführung von Container-Virtualisierte Anwendungen. Am Ende des Tages is docker nicht anderes als ein Program, dass uns erlaubt Anwendungen in Containers zu virtualisieren. Aber was zum Henker sind Virtualisierung und Containers. Sehr vereinfacht: Docker besteht grundsätzlich aus zwei Teilen eine Client-Anwendung (docker) und eine Server-Anwendung oder Service (dockerd)


## Virtualisierung

![virtualize](images/virtualize.png)

Notes: Virtualisierung ist die Ausführung einer virtuellen Instanz eines Computersystems in einer von der tatsächlichen Hardware abstrahierten Schicht. Virtualisierung ist die Bereitstellung einer Umgebung, um unterschiedliche Aufgaben getrennt voneinander ablaufen zu lassen. Beispiel VDIs in der DATEV: auf eine physikalischen Maschine werden unterschiedliche Desktops bereitgestellt. So sieht es aus als ob jeder von uns ein eigenen Rechner verwendet, aber in wirklichkeit arbeiten wir auf eine sog. VM. VM sind nicht die einzige Möglichkeit zur Virtualisierung.


## Container 

![container](images/container-snail.jpg)

Notes: Ein Container ist eine Software-Einheit, die Code und all seine Abhängigkeiten verpackt, um eine bestimmte Aufgabe zu erfüllen. Es gibt unterschiedliche Definitionen, darüber was ein Container ist. Ich habe mir diese herausgepickt, weil sie eine der für mich wichtigsten Eigenschaften von Container hervorhebt. Anwendung und Abhängigkeiten werden zusammengepackt. Einfach zu transferieren.


## Container vs. VM

![compare](images/containerVsVM.jpg)

Notes: Container sind eine Abstraktion auf der Anwendungsschicht, die Code und Abhängigkeiten zusammen verpackt. Mehrere Container können auf demselben Rechner laufen und sich den Betriebssystemkern mit anderen Containern teilen, wobei jeder Container als isolierte Prozesse im Benutzerbereich läuft. Container nehmen weniger Platz ein als VMs (Container-Images sind in der Regel mehrere zehn MB groß), können mehr Anwendungen verarbeiten und benötigen weniger VMs und Betriebssysteme.virtuelle Maschinen (VMs) sind eine Abstraktion der physischen Hardware, die einen Server in viele Server verwandelt. Der Hypervisor ermöglicht die Ausführung mehrerer VMs auf einer einzigen Maschine. Jede VM enthält eine vollständige Kopie eines Betriebssystems, der Anwendung, der erforderlichen Binärdateien und Bibliotheken - was Dutzende von GBs in Anspruch nimmt. VMs können auch langsam hochfahren.
