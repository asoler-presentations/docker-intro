# Einführung in docker und docker-compose

## Willkommen

## Thema

## Ich

## Was ist Docker

Docker ist eine Software zu Erstellung, Verwaltung und Ausführung von Container-Virtualisierte 
Anwendungen. 

Am Ende des Tages is docker nicht anderes als ein Program, dass uns erlaubt Anwendungen in 
Container zu virtualisieren. 

Aber was zum Henker sind Virtualisierung und Containers. 

## Virtualisierung

Virtualisierung ist die Ausführung einer virtuellen Instanz eines Computersystems in einer von 
der tatsächlichen Hardware abstrahierten Schicht. 

Virtualisierung ist die Bereitstellung einer Umgebung, um unterschiedliche Aufgaben getrennt 
voneinander ablaufen zu lassen. 

Beispiel VDIs in der DATEV: auf eine physikalische Maschine werden unterschiedliche Desktops 
bereitgestellt. So sieht es aus als ob jeder von uns einen eigenen Rechner verwendet, aber in 
wirklichkeit arbeiten wir auf eine sog. VM. 

VM sind nicht die einzige Möglichkeit zur Virtualisierung.

## Container

Ein Container ist eine Software-Einheit, die Code und all seine Abhängigkeiten verpackt, 
um eine bestimmte Aufgabe zu erfüllen. 

Es gibt unterschiedliche Definitionen, darüber was ein Container ist. 

Ich habe mir diese herausgepickt, weil sie eine der für mich wichtigsten Eigenschaften von 
Container hervorhebt. 

- Anwendung und Abhängigkeiten werden zusammengepackt.
- Einfach zu transferieren.

## Container vs VM

Container sind eine Abstraktion auf der Anwendungsschicht, die Code und Abhängigkeiten zusammen 
verpackt. Mehrere Container können auf demselben Rechner laufen und sich den Betriebssystemkern 
mit anderen Containern teilen, wobei jeder Container als isolierte Prozesse im Benutzerbereich 
läuft. Container nehmen weniger Platz ein als VMs (Container-Images sind in der Regel mehrere 
zehn MB groß), können mehr Anwendungen verarbeiten und benötigen weniger VMs und Betriebssysteme.

virtuelle Maschinen (VMs) sind eine Abstraktion der physischen Hardware, die einen Server in 
viele Server verwandelt. Der Hypervisor ermöglicht die Ausführung mehrerer VMs auf einer einzigen 
Maschine. Jede VM enthält eine vollständige Kopie eines Betriebssystems, der Anwendung, der 
erforderlichen Binärdateien und Bibliotheken - was Dutzende von GBs in Anspruch nimmt. 
VMs können auch langsam hochfahren.

## Docker-Plattform

Sehr vereinfacht: Docker besteht grundsätzlich aus zwei Teilen eine Client-Anwendung (docker) 
und eine Server-Anwendung oder Service (dockerd)

## Images

Ein Image ist eine Vorlage mit Anweisungen zum Erstellen eines Docker-Containers. 

Ein Image ist die Definition wie ein Container zu erstellen ist. Darin werden alle notwendige 
Informationen festgelegt, um den Container lauffähig zu bekommen.

Häufig basiert ein Image auf einem anderen Image, mit einigen zusätzlichen Anpassungen. 

Du kannst deine eigene Images erstellen oder nur die von anderen erstellten und in einer 
Registry veröffentlichten Images verwenden. 

## Container

Ein Docker-Container ist die lauffähige Instanz eines bestimmten Images. 

Jeder gestartete Docker Container bekommt eine eindeutige IP-Adresse. 

Container besitzen einen höhen Grad an Isolation: 

- Docker-Container sind von der außenwelt isoliert. Um sie von außen (Intranet / Internet) erreichbar zu machen nutzen wir Port-Weiterleitungen. 
- Docker-Container sind voneinander isoliert. Um sie miteinander kommunizieren zu lassen, verwenden wir Docker-Netzwerke. 
- Alle an Daten innerhalb des Containers durchgeführten Operationen gehen verloren, nachdem der Container gestoppt / gelöscht wird. Um Daten nicht zu verlieren, verwenden wir Docker-Volumes.

## Network

Um Kommunikation zwischen Container zu ermöglichen, verwenden wir Docker-Netzwerke. 

Ein Docker-Netzwerk ist ein virtuelles Netzwerk. 

Alle Container innerhalb eines Docker-Netzwerk können miteinander kommunizieren, wie normaler Rechner in einem echten Netzwerk.

## Volume

Daten werden in Volumes gespeichert. 

Volumes befinden sich auf dem Host-System. 

Volumes können von docker automatisch verwaltet werden oder von uns manuell.

## Compose

Compose ist ein Werkzeug zur Definition und Ausführung von Multi-Container-Docker-Anwendungen. 

Mit Compose verwenden wir eine YAML-Datei zur Konfiguration der Dienste (>=1) in unsere Anwendung. 

Anschließend können wir mit einem einzigen Befehl alle Dienste aus unsere 
Konfiguration heraus erstellen und starten.
