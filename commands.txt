Check das docker funktioniert:

docker run hello-world


Ein Linux container starten und damit interagieren:

docker run -ti ubuntu bash


Ein Webserver aufsetzen:

docker run --name my-website  -v /tmp/www:/usr/share/nginx/www -p 8080:80 nginx
